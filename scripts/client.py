#! /usr/bin/env python

import rospy

# Brings in the SimpleActionClient
import actionlib

# Brings in the messages used by the fibonacci action, including the
# goal message and the result message.
import control_msgs.msg
from trajectory_msgs.msg import JointTrajectoryPoint


def main():
  rospy.init_node("client")
  
  # Creates the SimpleActionClient, passing the type of the action
  # (FibonacciAction) to the constructor.
  client = actionlib.SimpleActionClient('/arm1/ur5_controller/follow_joint_trajectory', control_msgs.msg.FollowJointTrajectoryAction)

  # Waits until the action server has started up and started
  # listening for goals.
  client.wait_for_server()

  # Creates a goal to send to the action server.
  goal = control_msgs.msg.FollowJointTrajectoryGoal()
  goal.trajectory.joint_names = ['base', 'shoulder', 'elbow', 'wrist_1', 'wrist_2', 'wrist_3']
  goal.trajectory.points.append(JointTrajectoryPoint())
  goal.trajectory.points[0].positions = [5, -1.5, 0, -1.5, 0, 0]
  goal.trajectory.points[0].time_from_start = rospy.Duration(1)
  
  

  # Sends the goal to the action server.
  client.send_goal(goal)

  # Waits for the server to finish performing the action.
  client.wait_for_result()

  # Prints out the result of executing the action
  result = client.get_result()  # A FibonacciResult
  print(result)


if __name__ == '__main__':
  try:
    main()
  except rospy.ROSInterruptException:
    print("program interrupted before completion")
